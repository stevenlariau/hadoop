import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) throws IOException, InterruptedException {

        try (BufferedReader br = new BufferedReader(new FileReader("deploy.txt"))) {
            String addr;
            while ((addr = br.readLine()) != null) {
                ProcessBuilder pb = new ProcessBuilder("ssh", addr, "mkdir", "-p", "/tmp/lariau_s", ";", "exit");
                Process p = pb.start();
                p.waitFor(5, TimeUnit.SECONDS);

                if (!p.isAlive()) {
                    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                    StringBuilder builder = new StringBuilder();
                    String line = null;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                        builder.append(System.getProperty("line.separator"));
                    }
                    String result = builder.toString();

                    if (p.exitValue() == 0) {
                        System.out.println(addr + " : Success");
                        System.out.println(addr + ": Deploying code ...");

                        ProcessBuilder pb2 = new ProcessBuilder("scp", "/tmp/hadoop/slave.jar", addr + ":/tmp/lariau_s/slave.jar");
                        Process p2 = pb2.start();
                        p2.waitFor(5, TimeUnit.SECONDS);

                    } else {
                        System.out.println(addr + " : Error");
                    }
                } else {
                    System.out.println(addr + " : Error");
                }

            }
        }
    }
}