import java.awt.*;
import java.io.FileReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;


public class Main {


    public static class DictEntry
    {
        String word;
        ArrayList<String> ums;

        DictEntry(String word)
        {
            this.word = word;
            this.ums = new ArrayList<>();
        }
    }

    public static void main(String[] args) throws InterruptedException, IOException {

        Map<String, DictEntry> dict = new HashMap<>();


        BufferedReader br = new BufferedReader(new FileReader("deploy.txt"));

        String[] files = new String[] {
                "S0.txt",
                "S1.txt",
                "S2.txt"
        };

        int findex = 0;

        String addr;
        while ((addr = br.readLine()) != null) {
            ProcessBuilder pb1 = new ProcessBuilder("ssh", addr, "mkdir", "-p", "/tmp/lariau_s/splits", ";", "exit");
            Process p1 = pb1.start();
            p1.waitFor(5, TimeUnit.SECONDS);

            ProcessBuilder pb2 = new ProcessBuilder("scp", "-r", "/tmp/hadoop/splits", addr + ":/tmp/lariau_s/");
            Process p2 = pb2.start();
            p2.waitFor(5, TimeUnit.SECONDS);

            String filename = files[findex];
            ++findex;

            ProcessBuilder pb = new ProcessBuilder("ssh", "-T", addr, "java", "-jar", "/tmp/lariau_s/slave.jar",
                    "/tmp/lariau_s/splits/" + filename, ";", "exit");
            Process p = pb.start();
            p.waitFor(15, TimeUnit.SECONDS);

            if (!p.isAlive()) {
                BufferedReader reader = new BufferedReader(new InputStreamReader(p.getErrorStream()));
                StringBuilder builder = new StringBuilder();
                String line = null;
                while ((line = reader.readLine()) != null) {
                    builder.append(line);
                    builder.append(System.getProperty("line.separator"));
                }
                String result = builder.toString();

                BufferedReader reader2 = new BufferedReader(new InputStreamReader(p.getInputStream()));
                StringBuilder builder2 = new StringBuilder();
                String line2 = null;
                while ((line2 = reader2.readLine()) != null) {
                    builder2.append(line2);
                    builder2.append(System.getProperty("line.separator"));
                }
                String result2 = builder2.toString();

                if (p1.exitValue() == 0 && p.exitValue() == 0) {
                    System.out.println(filename + " " + addr);
                    //System.out.println(result);
                    //System.out.println(result2);

                    String[] words = result2.split("\\s+");
                    for (String w : words) {
                        if (dict.get(w) == null)
                            dict.put(w, new DictEntry(w));
                        DictEntry entry = dict.get(w);
                        entry.ums.add(filename);
                    }

                } else {
                    System.out.println(addr + ": Error");
                }
            }
        }

        for (DictEntry entry: dict.values())
        {
            String str = "word " + entry.word + " in ";
            for (String f : entry.ums)
                str += f + " ";
            System.out.println(str);
        }

        System.out.println("Map Done");
    }
}