import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {

    public static void main(String[] args) throws IOException {

        char fileN =  args[0].charAt(args[0].length() - 5);
        new File("/tmp/lariau_s/maps/").mkdirs();
        String outName = "/tmp/lariau_s/maps/UM" + fileN + ".txt";

        Set<String> keys = new HashSet<>();


        BufferedWriter bw = new BufferedWriter(new FileWriter(outName));

        try (BufferedReader br = new BufferedReader(new FileReader(args[0]))) {
            String line;
            while ((line = br.readLine()) != null) {

                for (String word : line.split("\\s+"))
                {
                    if (keys.add(word))
                        System.out.println(word);
                    bw.write(word + " 1\n");
                }
            }
        }

        bw.close();
    }
}